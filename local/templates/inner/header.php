<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
IncludeTemplateLangFile(__FILE__);?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?$APPLICATION->ShowHead();?>

    <?if (!isset($_GET["print_course"])):?>
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH."/print_style.css"?>" type="text/css" media="print" />
    <?else:?>
        <meta name="robots" content="noindex, follow" />
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH."/print_style.css"?>" type="text/css" />
    <?endif?>
    <script type="text/javascript">
        function ShowSwf(sSwfPath, width1, height1)
        {
            var scroll = 'no';
            var top=0, left=0;
            if(width1 > screen.width-10 || height1 > screen.height-28)
                scroll = 'yes';
            if(height1 < screen.height-28)
                top = Math.floor((screen.height - height1)/2-14);
            if(width1 < screen.width-10)
                left = Math.floor((screen.width - width1)/2);
            width = Math.min(width1, screen.width-10);
            height = Math.min(height1, screen.height-28);
            window.open('<?=SITE_TEMPLATE_PATH."/js/swfpg.php"?>?width='+width1+'&height='+height1+'&img='+sSwfPath,'','scrollbars='+scroll+',resizable=yes, width='+width+',height='+height+',left='+left+',top='+top);
        }
    </script>

    <title><?$APPLICATION->ShowTitle();?></title>

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH."/template_styles.css"?>" type="text/css" />
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH."/favicon.ico"?>" type="image/x-ico" />

    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/slides.min.jquery.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.carouFredSel-6.1.0-packed.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/functions.js"></script>


</head>
<body>
<?$APPLICATION->ShowPanel();?>
<div class="wrap">
    <div class="hd_header_area">
        <div class="hd_header">
            <table>
                <tr>
                    <td rowspan="2" class="hd_companyname">
                        <h1><a href="">Наименование компании</a></h1>
                    </td>
                    <td rowspan="2" class="hd_txarea">
                        <span class="tel">8 (495) 212-85-06</span>	<br/>
                        ����� ������ <span class="workhours"><?GetMessage('WORKING_TIME')?> 9-00 �� 18-00</span>
                    </td>
                    <td style="width:232px">
                        <form action="">
                            <div class="hd_search_form" style="float:right;">
                                <input placeholder="�����" type="text"/>
                                <input type="submit" value=""/>
                            </div>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 11px;">
							<span class="hd_singin"><a id="hd_singin_but_open" href="">����� �� ����</a>
							<div class="hd_loginform">
								<span class="hd_title_loginform">����� �� ����</span>
								<form name="" method="" action="">

									<input placeholder="�����"  type="text">
									<input  placeholder="������"  type="password">
									<a href="/" class="hd_forgotpassword">������ ������</a>

									<div class="head_remember_me" style="margin-top: 10px">
										<input id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" type="checkbox">
										<label for="USER_REMEMBER_frm" title="��������� ���� �� ���� ����������">��������� ����</label>
									</div>
									<input value="�����" name="Login" style="margin-top: 20px;" type="submit">
									</form>
								<span class="hd_close_loginform">�������</span>
							</div>
							</span><br>
                        <a href="" class="hd_signup">������������������</a>
                    </td>
                </tr>
            </table>
            <div class="nv_topnav">
                <ul>
                    <li><a href=""   class="menu-img-fon"  style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/nv_home.png);" ><span></span></a></li>
                    <li><a href=""><span>��������</span></a>
                        <ul>
                            <li><a href="">����� 1</a></li>
                            <li><a href="">����� 2</a></li>
                            <li><a href="">����� 3</a></li>
                            <li><a href="">����� 4</a></li>
                        </ul>
                    </li>
                    <li><a href=""><span>�������</span></a></li>
                    <li><a href=""><span>�������</span></a></li>
                    <li><a href=""><span>�����</span></a>
                        <ul>
                            <li><a href="">����� 1</a>
                                <ul>
                                    <li><a href="">����� 1</a></li>
                                    <li><a href="">����� 2</a></li>
                                </ul>
                            </li>
                            <li><a href="">����� 2</a></li>
                            <li><a href="">����� 3</a></li>
                            <li><a href="">����� 4</a></li>
                        </ul>
                    </li>
                    <li><a href=""><span>���������</span></a></li>
                    <li><a href=""><span>��������</span></a></li>
                    <div class="clearboth"></div>
                </ul>
            </div>
        </div>
    </div>

    <!--- //  header area --->
    <div class="bc_breadcrumbs">
        <ul>
            <li><a href="">�������</a></li>
            <li><a href="">������</a></li>
            <li><a href="">�������� � �������</a></li>
        </ul>
        <div class="clearboth"></div>
    </div>
    <div class="main_container page">
        <div class="mn_container">
            <div class="mn_content">
                <div class="main_post">
                    <div class="main_title">
                        <p class="title">��������� ��������</p>
                    </div>